<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * WEB VIEW ROUTES
 */

Route::get('/', ['as' => 'home', 'uses' => 'webController@index']);
Route::get('/home', ['as' => 'home', 'uses' => 'webController@index']);

Route::get('/about-us', ['as' => 'about.us', 'uses' => 'webController@about_us']);
Route::get('/gallery', ['as' => 'gallery', 'uses' => 'webController@gallery']);
Route::get('/contact', ['as' => 'contact', 'uses' => 'webController@contact']);



Route::get('/magazine-details/{id}/{name}', ['as' => 'magazine.details', 'uses' => 'webController@magazine_details']);

Route::get('magazine_details/{id}/read_magazine', ['as' => 'read_magazine', 'uses' => 'webController@read_magazine']);


Route::get('/login', ['as' => 'user.login', 'uses' => 'userLogin@index']);

Route::post('/loginCheck', ['as' => 'user.login.check', 'uses' => 'userLogin@ajax_login']);

Route::get('/logout', ['as' => 'logout', 'uses' => 'userLogin@logout']);

Route::get('/redirect', 'SocialLoginController@redirect');

Route::get('/callback', 'SocialLoginController@callback');

/*
 * Admin Routes
 */

Route::get('/admin_dashboard', ['as' => 'admin_dashboard', 'uses' => 'admin@index']);
Route::get('/admin_dashboard/profile', ['as' => 'admin.profile', 'uses' => 'admin@profile']);
Route::post('/admin_dashboard/profile/{image}', ['as' => 'profile.image', 'uses' => 'admin@profile']);
Route::post('/admin_dashboard/profile/{do_update}', ['as' => 'profile.update', 'uses' => 'admin@profile']);

/*
 * Magazine Edition CRUD
 */

Route::get('/admin_dashboard/edition', ['as' => 'edition', 'uses' => 'admin@edition']);
Route::post('/admin_dashboard/edition/{create}', ['as' => 'edition.create', 'uses' => 'admin@edition']);
Route::post('/admin_dashboard/edition/{do_update}/{id}', ['as' => 'edition.update', 'uses' => 'admin@edition']);
Route::get('/admin_dashboard/edition/{delete}/{id}', ['as' => 'edition.delete', 'uses' => 'admin@edition']);

/*
 * Magazine CRUD
 */

Route::get('/admin_dashboard/magazine', ['as' => 'magazine', 'uses' => 'admin@magazine']);
Route::post('/admin_dashboard/magazine/{create}', ['as' => 'magazine.create', 'uses' => 'admin@magazine']);
Route::post('/admin_dashboard/magazine/{do_update}/{id}', ['as' => 'magazine.update', 'uses' => 'admin@magazine']);
Route::get('/admin_dashboard/magazine/{delete}/{id}', ['as' => 'magazine.delete', 'uses' => 'admin@magazine']);


/*
 * !Admin Routes
 */
