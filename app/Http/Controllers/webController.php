<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;


class webController extends Controller
{
    public function index()
    {
        $data = array();
        $data['title'] = 'Home';
        $data['latest'] = DB::table('magazine')
            ->latest()
            ->join('mag_edition', 'magazine.edition', '=', 'mag_edition.edition_id')
            ->select('magazine.*', 'mag_edition.edition_name')
            ->paginate(12);
        $data['latest']->withPath('home');
        $page = view('frontend.dynamic_files.home', $data);
        return view('frontend.master', $data)->with('page', $page);
    }

    /*
     * BOOK DETAILS FUNCTION
     */

    public function magazine_details($id)
    {
        $data = array();
        $mag_id = base64_decode($id);
        $data['book_details'] = DB::table('magazine')
            ->where('magazine_id', $mag_id)
            ->join('mag_edition', 'magazine.edition', '=', 'mag_edition.edition_id')
            ->select('magazine.*', 'mag_edition.edition_name')
            ->first();
        $data['title'] = $data['book_details']->name . ' || Magazine';

        $data['latest'] = DB::table('magazine')
            ->latest()
            ->limit(5)
            ->join('mag_edition', 'magazine.edition', '=', 'mag_edition.edition_id')
            ->select('magazine.*', 'mag_edition.edition_name')
            ->get();

        $page = view('frontend.dynamic_files.magazine_details', $data);
        return view('frontend.master', $data)->with('page', $page);
    }

    public function read_magazine($id)
    {
        $mag_id = base64_decode($id);
        $data['book_details'] = DB::table('magazine')
            ->where('magazine_id', $mag_id)
            ->join('mag_edition', 'magazine.edition', '=', 'mag_edition.edition_id')
            ->select('magazine.*', 'mag_edition.edition_name')
            ->first();
        $data['title'] = $data['book_details']->name . ' || Magazine';

        return view('frontend.dynamic_files.read_magazine',$data);
    }


    public function about_us()
    {
        $data = array();
        $data['title'] = 'About Us';

        $data['latest'] = DB::table('magazine')
            ->latest()
            ->limit(5)
            ->join('mag_edition', 'magazine.edition', '=', 'mag_edition.edition_id')
            ->select('magazine.*', 'mag_edition.edition_name')
            ->get();

        $page = view('frontend.dynamic_files.about_us',$data);
        return view('frontend.master', $data)->with('page', $page);
    }

    public function gallery()
    {
        $data = array();
        $data['title'] = 'About Us';

        $data['latest'] = DB::table('magazine')
            ->latest()
            ->limit(12)
            ->get();

        $page = view('frontend.dynamic_files.gallery',$data);
        return view('frontend.master', $data)->with('page', $page);
    }

    public function contact()
    {
        $data = array();
        $data['title'] = 'Contact';

        $page = view('frontend.dynamic_files.contact',$data);
        return view('frontend.master', $data)->with('page', $page);
    }
}
