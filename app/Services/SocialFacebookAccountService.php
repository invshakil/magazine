<?php

namespace App\Services;
use App\SocialFacebookAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;
use Illuminate\Support\Facades\DB;

class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialFacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();


        if ($account) {
            $id = $providerUser->getId();

            $info = DB::table('social_facebook_accounts')
                ->join('users', 'social_facebook_accounts.user_id', '=', 'users.id')
                ->where('social_facebook_accounts.provider_user_id',$id)
                ->first();

            if ($info->image_path == NULL)
            {
                $user_id = $info->id;
                $data = array(
                    'name' => $providerUser->getName(),
                    'image_path' => $providerUser->avatar_original,
                    'fb_id' => $providerUser->profileUrl
                );

                DB::table('users')->where('id',$user_id)->update($data);

                return $account->user;
            }
            else
            {
                return $account->user;
            }



        } else {

            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'password' => md5(rand(1,10000)),
                    'image_path' => $providerUser->avatar_original,
                    'fb_id' => $providerUser->profileUrl
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}