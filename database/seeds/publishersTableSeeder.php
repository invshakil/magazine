<?php

use Illuminate\Database\Seeder;

class publishersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('publishers')->insert([
            'publishers_name' => 'Seba Prokashoni',
            'address' => 'Karwan Bazar',
            'logo' => 'http://apps.amarphonebook.com/largeImage/1436588396.jpg',
            'phone' => '0178323123',
            'email' => 'seba.prokasoni@gmail.com',
            'website' => 'sebaprokasoni.com',
            'status' => 1
        ]);

        DB::table('publishers')->insert([
            'publishers_name' => 'Boichitro Prokashoni',
            'address' => 'Kakrail',
            'logo' => 'http://www.boichitrobd.com/images/BoiChitroLogo.png',
            'phone' => '017233123',
            'email' => 'boichitrobd@gmail.com',
            'website' => 'boichitrobd.com',
            'status' => 1
        ]);
    }
}
