<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'Shakil',
            'email' => 'admin@gmail.com',
            'password' => md5('123456'),
            'user_type' => ('1')
        ]);

        DB::table('users')->insert([
            'name' => 'Khairul',
            'email' => 'author@gmail.com',
            'password' => md5('123456'),
            'user_type' => ('2')
        ]);

        DB::table('users')->insert([
            'name' => 'Touker',
            'email' => 'publisher@gmail.com',
            'password' => md5('123456'),
            'user_type' => ('3')
        ]);

        DB::table('categories')->insert([
            'category_name' => 'Thriller',
            'category_description' => 'All About Thriller',
            'status' => 1
        ]);
    }
}
