<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Neon Admin Panel"/>
    <meta name="author" content=""/>

    <title>@yield('title')</title>

    @include('backend.includes.include_top')


</head>
<body class="page-body skin-cafe" data-url="http://neon.dev">

<div class="page-container">
    <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    @include('backEnd.'.$account_type.'.'.'navigation')
    <div class="main-content">

        <div class="row">

            <!-- Profile Info and Notifications -->
            <div class="col-md-6 col-sm-8 clearfix">

                <ul class="user-info pull-left pull-none-xsm">

                    <!-- Profile Info -->
                    <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php
                                $id = Session::get('id');
                                $image = DB::table('users')->where('id', $id)->value('image_path');
                            ?>
                            <img src="<?php echo URL::to($image); ?>" alt="" class="img-circle" width="44" />
                            {{ Session::get('name') }}
                        </a>

                        <ul class="dropdown-menu">

                            <!-- Reverse Caret -->
                            <li class="caret"></li>

                            <!-- Profile sub-links -->
                            <li>
                                <a href="{{ route('admin.profile') }}">
                                    <i class="entypo-user"></i>
                                    Edit Profile
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>

            <!-- Raw Links -->
            <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                <ul class="list-inline links-list pull-right">

                    <li>
                        <a href="{{ route('logout') }}">
                            Log Out <i class="entypo-logout right"></i>
                        </a>
                    </li>
                </ul>

            </div>

        </div>

        <hr/>


        <div class="col-md-12">

            @yield('main_content')

        </div>

        <!-- Footer -->
        <footer class="main">

            &copy; 2014 <strong>Software</strong> Developed by <a href="http://fb.com/inverse.shakil"
                                                                  target="_blank">Techno-71</a>

        </footer>
    </div>


</div>

{{--@include('backend.includes.modal')--}}

@include('backend.includes.include_bottom')
</body>
</html>