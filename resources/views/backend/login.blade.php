<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Neon Admin Panel"/>
    <meta name="author" content=""/>

    <title>{{ $page_title }}</title>


    <link rel="stylesheet"
          href="{{ URL::to('public') }}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="{{ URL::to('public') }}/assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="{{ URL::to('public') }}/assets/css/bootstrap.css">
    <link rel="stylesheet" href="{{ URL::to('public') }}/assets/css/neon-core.css">
    <link rel="stylesheet" href="{{ URL::to('public') }}/assets/css/neon-theme.css">
    <link rel="stylesheet" href="{{ URL::to('public') }}/assets/css/neon-forms.css">
    <link rel="stylesheet" href="{{ URL::to('public') }}/assets/css/custom.css">

    <script src="{{ URL::to('public') }}/assets/js/jquery-1.11.0.min.js"></script>

    <!--[if lt IE 9]>
    <script src="{{ URL::to('public') }}/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ URL::to('public') }}/assets/images/favicon.png">

</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
    var baseurl = '{{ route('home') }}';
</script>

<div class="login-container">

    <div class="login-header login-caret">

        <div class="login-content" style="width:100%;">

            <a href="{{ URL::to('/') }}" class="logo">
                <img src="{{URL::to('/public/assets/')}}/images/highlights-6-crop.png" height="60" alt=""/>
            </a>

            <p class="description">
            <h2 style="color:#cacaca; font-weight:100;">
                <?php echo $page_title;?>
            </h2>
            </p>

            <!-- progress bar indicator -->
            <div class="login-progressbar-indicator">
                <h3>43%</h3>
                <span>logging in...</span>
            </div>
        </div>

    </div>

    <div class="login-progressbar">
        <div></div>
    </div>

    <div class="login-form">
        <div class="login-content">
            <div class="form-group">
                <div class="pull-left" style="padding-top: 5px">
                    <a href="{{url('/redirect')}}"><img src="{{ URL::to('public/fb-login.png') }}" width="170"></a>
                </div>
                <div class="pull-right">
                    <a href=""><img src="{{ URL::to('public/google-login.png') }}" height="45"></a>
                </div>
            </div>
        </div>

        <div class="login-content">

            @if (count($errors) > 0)
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }} {{ Session::put('message',null) }}</div>
                @endif
            </div>

            <div class="form-login-error">
                <h3>Invalid login</h3>
                <p>Please enter correct email and password!</p>
            </div>

            {{--<form method="post" role="form" id="form_login">--}}
            <form method="post" role="form" action="{{ URL::to('/loginCheck') }}">

                {{ csrf_field() }}

                <div class="form-group">

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>

                        <input type="text" class="form-control" name="email" id="email" placeholder="Email"
                               autocomplete="off" data-mask="email"/>
                    </div>

                </div>

                <div class="form-group">

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>

                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"
                               autocomplete="off"/>
                    </div>

                </div>

                <div class="form-group">
                    @if(Session::has('exception'))
                        <div class="alert alert-danger">{{ Session::get('exception') }} {{ Session::put('exception',null) }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="entypo-login"></i>
                        Login
                    </button>
                </div>


            </form>

            <!--<div class="login-bottom-links">
                <a href="#" class="link">Forgot your password?</a>
            </div>-->

        </div>

    </div>

</div>


<!-- Bottom Scripts -->
<script src="{{ URL::to('public') }}/assets/js/gsap/main-gsap.js"></script>
<script src="{{ URL::to('public') }}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="{{ URL::to('public') }}/assets/js/bootstrap.js"></script>
<script src="{{ URL::to('public') }}/assets/js/joinable.js"></script>
<script src="{{ URL::to('public') }}/assets/js/resizeable.js"></script>
<script src="{{ URL::to('public') }}/assets/js/neon-api.js"></script>
<script src="{{ URL::to('public') }}/assets/js/jquery.validate.min.js"></script>
<script src="{{ URL::to('public') }}/assets/js/neon-login.js"></script>
<script src="{{ URL::to('public') }}/assets/js/neon-custom.js"></script>
<script src="{{ URL::to('public') }}/assets/js/neon-demo.js"></script>

</body>
</html>