@extends('frontend.master')

@section('main_content')
    <div class="container">
        <div class="page_title_area row">
            <div class="col-md-12">
                <div class="bredcrumb">
                    <ul>
                        <li><a href="{{url('/')}}">HOME</a>
                        </li>
                        <li class="active"><a href="#">Details</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="single-produce-page no-sidebar">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="single-product-details clearfix">
                        <div class="col-md-4">
                            <div class="single-slider-item">
                                <img src="{{url($book_details->image)}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="right-content">
                                <div class="product-info">
                                    <h2>Book Name: {{ $book_details->name }}</h2>

                                    <div class="price">
                                        <h3><i class="fa fa-bookmark" aria-hidden="true"></i>
                                            Edition: {{ $book_details->edition_name }}
                                        </h3>
                                    </div>

                                    <div class="actions">
                                        <ul>
                                            <li><a class="add-cart" href="{{url('magazine_details/'.base64_encode($book_details->magazine_id).'/read_magazine')}}"><span><span
                                                                class="fa fa-check-circle"></span></span> Read This Magazine</a>
                                            </li>
                                        </ul>
                                    </div>

                                    <br/><br/>

                                    <br/>
                                    <div class="container">
                                        <h2>MAGAZINE DESCRIPTION</h2>
                                        {!! $book_details->description !!}
                                    </div>

                                    <br/>

                                    <div class="actions">
                                        <div class="container">
                                            <h2>Share This Book</h2>
                                            <div class="social">
                                                <a href="http://www.facebook.com/sharer.php?u={{url()->current()}}"
                                                   id="share-fb" class="sharer button" target="_blank"><i
                                                            class="fa fa-3x fa-facebook-square"></i></a>
                                                <a href="http://twitter.com/home?status={{url()->current()}}"
                                                   id="share-tw" class="sharer button" target="_blank"><i
                                                            class="fa fa-3x fa-twitter-square"></i></a>
                                                <a href="http://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}"
                                                   id="share-li" class="sharer button" target="_blank"><i
                                                            class="fa fa-3x fa-linkedin-square"></i></a>
                                                <a href="https://plus.google.com/share?url={{url()->current()}}"
                                                   id="share-gp" class="sharer button" target="_blank"><i
                                                            class="fa fa-3x fa-google-plus-square"></i></a>
                                                <a href="#"
                                                   id="share-em" class="sharer button" target="_blank"><i
                                                            class="fa fa-3x fa-envelope-square"></i></a>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        @include('frontend.dynamic_files.magazine_details.recommended')
    </div>

@endsection