<div class="latest-items">
    <div class="tab-header">
        <div class="heading-title">
            <h3 class="title-text">LATEST Magazine</h3>
        </div>
        <!--/.tab-item-->
    </div>
    <!--/.tab-header-->
    <div class="tab-content row">
        <div id="new" class="tab-pane fade in active">

            @foreach($latest->chunk(4) as $resultChunk)
                <div class="container">
                    <div class="row">
                        @foreach($resultChunk as $item)
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="product-single">
                                    <div class="product-thumb">
                                        <img class="img-responsive" alt="Single product"
                                             src="{{URL::to ($item->image)}}" width="220">
                                        <div class="actions">
                                            <ul>
                                                <li><a class="add-cart"
                                                       href="{{url('/magazine-details/'.base64_encode($item->magazine_id).'/'.str_replace(' ','-',$item->name) )}}"><span><span
                                                                    class="fa fa-check-circle"></span></span> Check
                                                        Details</a>
                                                </li>

                                                <li class="pull-right"><a class="zoom"
                                                                          href="{{URL::to ($item->image)}}"><span
                                                                class="arrow_expand"></span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!--/.product-thumb-->
                                    <div class="product-info">
                                        <h2>{{ $item->name }}</h2>
                                        <div class="price">
                                            <div class="pull-left">
                                                <i class="fa fa-archive" aria-hidden="true"></i>
                                                Edition: {{$item->edition_name}}
                                            </div>
                                        </div>
                                    </div>
                                    <!--/.product-info-->
                                </div>
                                <!--/.product-single-->
                            </div>
                        @endforeach

                    </div>
                </div>

        @endforeach
                <div class="paginations text-center">
                    {{ $latest->render() }}
                </div>
        <!--/.col-md-3-->
        </div>

        <!--/.new-->
    </div>
</div>