@extends('frontend.master')

@section('title')
    Contact
@endsection

@section('main_content')
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="#">Home</a>
                            </li>
                            <li class="active"><a href="#">contact page</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->

            <!--contact-page-->
            <div class="content contact-page">
                <div class="row">
                    <!--Groham-Supports-->
                    <div class="groham-contact clearfix">
                        <div class="col-md-4 col-sm-6">
                            <div class="single-support">
                                <div class="support-img">
                                    <img alt="icon" src="{{ url('public') }}/web_assets/assets/images/contact-icon1.png">
                                </div>
                                <div class="hover-support-img">
                                    <img alt="icon" src="{{ url('public') }}/web_assets/assets/images/contact-icon-1.png">
                                </div>
                                <div class="support-text">
                                    <h3>Phone Number</h3>
                                    <p>+8801928-686294</p>
                                </div>
                            </div>
                            <!--/.single-support-->
                        </div>
                        <!--/.col-md-4-->
                        <div class="col-md-3 col-sm-6">
                            <div class="single-support">
                                <div class="support-img">
                                    <img alt="icon" src="{{ url('public') }}/web_assets/assets/images/contact-icon-211.png">
                                </div>
                                <div class="hover-support-img">
                                    <img alt="icon" src="{{ url('public') }}/web_assets/assets/images/contact-iconhover-2.png">
                                </div>
                                <div class="support-text">
                                    <h3>e-mail address</h3>
                                    <a href="mailto:ikmohon@gmail.com">ikmohon@gmail.com</a>
                                </div>
                            </div>
                            <!--/.single-support-->
                        </div>
                        <!--/.col-md-4-->
                        <div class="col-md-5 col-sm-12">
                            <div class="single-support">
                                <div class="support-img pull-left">
                                    <img alt="icon" src="{{ url('public') }}/web_assets/assets/images/contact-icon3.png">
                                </div>
                                <div class="hover-support-img">
                                    <img alt="icon" src="{{ url('public') }}/web_assets/assets/images/contact-icon-3.png">
                                </div>
                                <div class="support-text pull-right">
                                    <h3>Contact Address</h3>
                                    <p>Sishu Kanon, 307 Ulon Road, East Rampura, Dhaka-1219</p>
                                </div>
                            </div>
                            <!--/.single-support-->
                        </div>
                        <!--/.col-md-4-->
                    </div>
                    <!--/.groham-supports-->
                    <div class="col-md-12">
                        <div class="contact login">
                            <div class="heading-title">
                                <h3 class="reply-title">leave a reply</h3>
                            </div>
                            <!--/.heading-title-->
                            <form class="contact-form clearfix" action="#">
                                <div class="col-md-3 col-sm-6">
                                    <div class="your-name">
                                        <label for="your-name">Name <span class="required">*</span>
                                        </label>
                                        <br>
                                        <input class="input-field" type="text" name="your-name" value="" id="your-name">
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-3 col-sm-6">
                                    <div class="email">
                                        <label for="your-email">Email <span class="required">*</span>
                                        </label>
                                        <br>
                                        <input class="input-field" type="email" name="your-email" value=""
                                               id="your-email">
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-3 col-sm-6">
                                    <div class="email">
                                        <label for="your-subject">Subject</label>
                                        <br>
                                        <input class="input-field" type="text" name="your-subject" value=""
                                               id="your-subject">
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-3 col-sm-6">
                                    <div class="your-website">
                                        <label for="your-website">Website</label>
                                        <input class="input-field" type="text" name="your-website" value=""
                                               id="your-website">
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-12">
                                    <div class="your-message">
                                        <label for="your-message">Your Message</label>
                                        <br>
                                        <textarea name="your-message" cols="10" rows="6" id="your-message"></textarea>
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn-submit btn-hover">Submit</button>
                                </div>
                            </form>
                            <!--/.contant-form-->
                        </div>
                        <!--/.contant-->
                    </div>
                    <!--/.col-md-12-->
                </div>
                <!--end of row-->
            </div>
            <!--/contact-pate-->
        </div>
        <!--/.container-->
    </div>

@endsection