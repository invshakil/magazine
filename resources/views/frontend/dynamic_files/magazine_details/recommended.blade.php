<div class="latest-items">
    <div class="tab-header">
        <div class="heading-title">
            <h3 class="title-text">LATEST Magazine</h3>
        </div>
        <!--/.tab-item-->
    </div>
    <!--/.tab-header-->
    <div class="tab-content row">
        <div id="new" class="tab-pane fade in active">

            <div class="clients">
                <ul class="client-carousel">
                    @foreach($latest->all() as $item)
                        <li>
                            <a href="{{url('/magazine-details/'.base64_encode($item->magazine_id).'/'.str_replace(' ','-',$item->name) )}}"><img
                                        alt="{{ $item->name }}" src="{{URL::to ($item->thumbnail)}}">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>

        <!--/.col-md-3-->
        </div>

        <!--/.new-->
    </div>
</div>