@extends('frontend.master')

@section('title')
    HOME || Magazine
@endsection

@section('main_content')

    <div class="container">


        <!--Latest ebook -->
        @include('frontend.dynamic_files.home_files.latest')
        <!--/.Latest ebook-->


    </div>
@endsection