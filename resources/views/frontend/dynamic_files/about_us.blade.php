@extends('frontend.master')

@section('title')
    About Us
@endsection

@section('main_content')
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="#">Home</a>
                            </li>
                            <li class="active"><a href="#">About Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->
            <div class="about01">
                <!-- about shop area -->
                <div class="about_our_shop_area">
                    <div class="row">
                        <div class="col-md-7 col-sm-12">
                            <div class="about_shop_img">
                                <img class="img-responsive" src="{{ url('public') }}/web_assets/assets/images/about_shop-1.jpg" alt="about shop"/>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12">
                            <div class="about_shop_content">
                                <h3>Groham Fashion Outlet Store</h3>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                    tempor invidunt ut labore et dolore magna aliqu erat, sed diam voluptua. Nam liber
                                    tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim
                                    placerat facer possim assum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                    minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                                    aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.about shop area-->

                <!--about team area-->
                <div class="about_our_team_area">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="heading-title">
                                <h3 class="title-text">MEET OUR TEAM</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="single_team_member">
                                <div class="team-image">
                                    <img class="img-responsive" src="{{ url('public') }}/web_assets/assets/images/team/member-1.jpg" alt="team"/>
                                    <div class="prev-image">
                                        <a class="zoom" href="{{ url('public') }}/web_assets/assets/images/team/member-1.jpg">
                                            <img src="{{ url('public') }}/web_assets/assets/images/plus-white.png" alt="plus">
                                        </a>
                                    </div>
                                </div>
                                <h5>John Warlock</h5>
                                <p>Creative Director</p>
                            </div>
                            <!--/.single team member-->
                        </div>
                        <!--/.col-md-3-->
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="single_team_member">
                                <div class="team-image">
                                    <img class="img-responsive" src="{{ url('public') }}/web_assets/assets/images/team/member-2.jpg" alt="team"/>
                                    <div class="prev-image">
                                        <a class="zoom" href="{{ url('public') }}/web_assets/assets/images/team/member-2.jpg">
                                            <img src="{{ url('public') }}/web_assets/assets/images/plus-white.png" alt="plus">
                                        </a>
                                    </div>
                                </div>
                                <h5>Marcus Kepernick</h5>
                                <p>Product Reseller</p>
                            </div>
                            <!--/.single team member-->
                        </div>
                        <!--/.col-md-3-->
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="single_team_member">
                                <div class="team-image">
                                    <img class="img-responsive" src="{{ url('public') }}/web_assets/assets/images/team/member-3.jpg" alt="team"/>
                                    <div class="prev-image">
                                        <a class="zoom" href="{{ url('public') }}/web_assets/assets/images/team/member-3.jpg">
                                            <img src="{{ url('public') }}/web_assets/assets/images/plus-white.png" alt="plus">
                                        </a>
                                    </div>
                                </div>
                                <h5>Kevin Karter</h5>
                                <p>Shop Assitant</p>
                            </div>
                            <!--/.single team member-->
                        </div>
                        <!--/.col-md-3-->
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="single_team_member">
                                <div class="team-image">
                                    <img class="img-responsive" src="{{ url('public') }}/web_assets/assets/images/team/member-4.jpg" alt="team"/>
                                    <div class="prev-image">
                                        <a class="zoom" href="{{ url('public') }}/web_assets/assets/images/team/member-4.jpg">
                                            <img src="{{ url('public') }}/web_assets/assets/images/plus-white.png" alt="plus">
                                        </a>
                                    </div>
                                </div>
                                <h5>Peter Downing</h5>
                                <p>Sales Manager</p>
                            </div>
                            <!--/.single team member-->
                        </div>
                        <!--/.col-md-3-->
                    </div>
                    <!--/.row-->
                </div>
                <!--/.about team area-->

                <!--our experiences area-->
                <div class="our_experiences_area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-title">
                                <h3 class="title-text">WHAT WE DO</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 experience_content">
                            <p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                                sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
                                consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
                                magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
                                ea rebum.</p>

                            <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem
                                ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore
                                dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita
                                ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est
                                Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                                diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
                        </div>
                        <div class="col-md-6 col-sm-12 barWrapper">
                            <div class="single-progress">
                                <h5 class="progressText">modern clothes design</h5>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0"
                                         aria-valuemax="98" style="width: 87%">
                                        <span class="bar_over">90%</span>
                                    </div>
                                </div>
                            </div>
                            <!--/.single progress-->

                            <div class="single-progress">
                                <h5 class="progressText">Reselling Latest Brands</h5>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="92" aria-valuemin="0"
                                         aria-valuemax="100" style="width: 90%">
                                        <span class="bar_over">92%</span>
                                    </div>
                                </div>
                            </div>
                            <!--/.single progress-->

                            <div class="single-progress">
                                <h5 class="progressText">worldwide marketing</h5>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="72" aria-valuemin="0"
                                         aria-valuemax="100" style="width: 72%">
                                        <span class="bar_over">72%</span>
                                    </div>
                                </div>
                            </div>
                            <!--/.single progress-->

                            <div class="single-progress">
                                <h3 class="progressText">fashion shows</h3>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="84" aria-valuemin="0"
                                         aria-valuemax="100" style="width: 84%">
                                        <span class="bar_over">84%</span>
                                    </div>
                                </div>
                            </div>
                            <!--/.single progress-->
                        </div>
                        <!--/.bar wrapper-->
                    </div>
                    <!--/.row-->
                </div>
                <!--/.our experiences area-->
            </div>

            <div class="latest-items">
                <div class="tab-header">
                    <div class="heading-title">
                        <h3 class="title-text">LATEST Magazine</h3>
                    </div>
                    <!--/.tab-item-->
                </div>
                <!--/.tab-header-->
                <div class="tab-content row">
                    <div id="new" class="tab-pane fade in active">

                        <div class="clients">
                            <ul class="client-carousel">
                                @foreach($latest->all() as $item)
                                    <li>
                                        <a href="{{url('/magazine-details/'.base64_encode($item->magazine_id).'/'.str_replace(' ','-',$item->name) )}}"><img
                                                    alt="{{ $item->name }}" src="{{URL::to ($item->thumbnail)}}">
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <!--/.col-md-3-->
                    </div>

                    <!--/.new-->
                </div>
            </div>
            <!--/.client -->
        </div>
        <!--/.container-->
    </div>
@endsection