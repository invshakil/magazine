<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $book_details->name }}</title>
    {{--<script async="" src="{{ url('public') }}/flipbook/assets/analytics.js"></script>--}}
    <script>
        // Break out iframe on iOS cause safari expand iframe to fit the content, thus making
        // the lightbox bigger than the browser window.
        // We need to check for MSStream because Microsoft injected the word iPhone in IE11's userAgent therefore we need to exclude it.
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        if (iOS && top.location != self.location) {
            top.location = self.location.href;
        }
    </script>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ url('public') }}/flipbook/assets/bootstrap.css" type="text/css">

    <!-- Custom Fonts -->
    <link href="{{ url('public') }}/flipbook/assets/css.css" rel="stylesheet" type="text/css">
    <link href="{{ url('public') }}/flipbook/assets/css_002.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ url('public') }}/flipbook/assets/font-awesome.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{ url('public') }}/flipbook/assets/animate.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ url('public') }}/flipbook/assets/creative.css" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="">


<section id="services">
    <div class="container">
        <div class="row">
            <a class="btn btn-info" href="{{url('/')}}">Go Home</a>
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">{{ $book_details->name }}</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div id="book-trigger" class="col-lg-12 col-md-12 text-center">
                <div class="service-box">
                    <img class="book-thumb" src="{{ url($book_details->thumbnail) }}">
                    <h3>Edition: {{ $book_details->edition_name }}</h3>
                    <p class="text-muted"> pdf file</p>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="no-padding" id="portfolio">
</section>


<section id="contact">
</section>

<div style="display: none">
    <div id="book1"></div>


    <img src="{{ url('public') }}/flipbook/assets/wood.jpg" style="visibility: hidden">
</div>
<style>
    .hidden-FIXME {
        display: none;
    }

    #portfolio {
        display: none;
    }

    header {
        display: none;
    }

    #contact {
        display: none;
    }

    #about {
        display: none;
    }

    .navbar-header {
        width: 100% !important;
    }

    .navbar-brand {
        padding-right: 0;
        font-size: 14px !important;
    }

    #nav-buy-now {
        color: black;
        float: right;
        font-size: 14px;
        font-weight: 700;
        margin-right: -10px;
    }

    @media (min-width: 768px) {
        #nav-buy-now {
            margin-right: 0px;
        }
    }

    #bs-example-navbar-collapse-1 {
        display: none;
    }

    .wowbook {
        font-family: "Open Sans", "Helvetica Neue", Arial, sans-serif;
    }

    .wowbook-page-content {
        padding: 1.5em;
    }

    .wowbook ul {
        padding-left: 1em;
    }

    .book-thumb {
        height: 150px;
        box-shadow: 0 0 3px rgba(0, 0, 0, 0.5)
    }

    #book1-trigger, #book-trigger, #book3-trigger {
        cursor: pointer;
    }

    #book1-trigger:hover, #book-trigger:hover, #book3-trigger:hover {
        background: #f8f8f8;
    }

    .wowbook-lightbox > .wowbook-close {
        background: transparent !important;
        border: none !important;
        color: #222 !important;
        font-size: 2.5em;
    }

    .wowbook-lightbox > .wowbook-close:hover {
        background: #444 !important;
        color: white !important;
        border-radius: 3px;
    }

    .lightbox-images1 .wowbook-book-container {
        background: #6d6b92; /* Old browsers */
        background: -moz-radial-gradient(center, ellipse cover, #ffffff 0%, #6d6b92 100%); /* FF3.6-15 */
        background: -webkit-radial-gradient(center, ellipse cover, #ffffff 0%, #6d6b92 100%); /* Chrome10-25,Safari5.1-6 */
        background: radial-gradient(ellipse at center, #ffffff 0%, #6d6b92 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    }

    .lightbox-images1 > .wowbook-close,
    .lightbox-images2 > .wowbook-close {
        color: #ccc !important;
    }

    .lightbox-images2 .wowbook-book-container {
        background: #1E2831; /* Old browsers */
        background: -moz-radial-gradient(center, ellipse cover, #ffffff 0%, #1E2831 100%); /* FF3.6-15 */
        background: -webkit-radial-gradient(center, ellipse cover, #ffffff 0%, #1E2831 100%); /* Chrome10-25,Safari5.1-6 */
        background: radial-gradient(ellipse at center, #ffffff 0%, #1E2831 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    }

    .lightbox-pdf .wowbook-book-container {
        background: #e5e5e5 url(./img/bg-lightbox-pdf.png); /* Old browsers */
        background: #e5e5e5 -moz-radial-gradient(center, ellipse cover, #ffffff 20%, #bbbbbb 100%); /* FF3.6-15 */
        background: #e5e5e5 -webkit-radial-gradient(center, ellipse cover, #ffffff 20%, #bbbbbb 100%); /* Chrome10-25,Safari5.1-6 */
        background: #e5e5e5 radial-gradient(ellipse at center, #ffffff 20%, #bbbbbb 100%); /* W3C, IE10+, FF16+, Chrome26+,Opera12+, Safari7+*/
    }

    .lightbox-html .wowbook-book-container {
        background: url({{ url('public') }}/flipbook/assets/wood.jpg);
    }

    .lightbox-html .wowbook-toolbar {
        margin-top: 1em; /* FIXME */
        box-sizing: content-box !important;
    }

    .lightbox-html .wowbook-controls {
        border-radius: 6px;
        width: auto;
    }

    .lightbox-html.wowbook-mobile .wowbook-toolbar {
        margin: 0;
    }

    .lightbox-html.wowbook-mobile .wowbook-controls {
        border-radius: 0;
        width: 100%;
    }

    hr {
        max-width: 450px;
    }
</style>

<!-- jQuery -->
<script src="{{ url('public') }}/flipbook/assets/jquery_003.js"></script>
<script>
    imageBook = ["1", "8"][Math.floor(Math.random() * 2)];
    imageBookPath = "./img/magazine_template_0" + imageBook;
    $("#book1-trigger .book-thumb").attr("src", imageBookPath + "/image_000.jpg")
</script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ url('public') }}/flipbook/assets/bootstrap.js"></script>

<!-- Plugin JavaScript -->
<script src="{{ url('public') }}/flipbook/assets/jquery_002.js"></script>
<script src="{{ url('public') }}/flipbook/assets/jquery.js"></script>
<script src="{{ url('public') }}/flipbook/assets/wow.js"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ url('public') }}/flipbook/assets/creative.js"></script>

<link rel="stylesheet" href="{{ url('public') }}/flipbook/assets/wow_book.css" type="text/css">
<style>
    .wowbook-right .wowbook-gutter-shadow {
        background-image: url("{{ url('public') }}/flipbook/assets/page_right_background.png");
        background-position: 0 0;
        width: 75px;
    }

    .wowbook-left .wowbook-gutter-shadow {
        background-image: url("{{ url('public') }}/flipbook/assets/page_left_background.png");
        opacity: 0.5;
        width: 60px;
    }

    .wowbook-control-currentPage {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
    }
</style>
<script type="text/javascript" src="{{ url('public') }}/flipbook/assets/pdf.js"></script>
<script type="text/javascript" src="{{ url('public') }}/flipbook/assets/wow_book.js"></script>
<script type="text/javascript">
    $(function () {

        function fullscreenErrorHandler() {
            if (self != top) return "The frame is blocking full screen mode. Click on 'remove frame' button above and try to go full screen again."
        }

        // imageBook = ["1", "8"][ Math.floor(Math.random()*2)];
        // imageBookPath = "./img/magazine_template_0"+imageBook+"/";
        // $("#book1-trigger .book-thumb").attr("src", imageBookPath+"image_000.jpg")

        
        var optionsbook = {
            height: 1024
            ,
            width: 725 * 2
            // ,maxWidth : 800
            // ,maxHeight : 400
            ,
            pageNumbers: false

            ,
            pdf: "{{ url($book_details->file) }}"
            ,
            pdfFind: true
            ,
            pdfTextSelectable: true

            ,
            lightbox: "#book-trigger"
            ,
            lightboxClass: "lightbox-pdf"
            ,
            centeredWhenClosed: true
            ,
            hardcovers: true
            ,
            curl: false
            ,
            toolbar: "lastLeft, left, currentPage, right, lastRight, find, toc, zoomin, zoomout, flipsound, fullscreen, thumbnails"
            ,
            thumbnailsPosition: 'bottom'
            ,
            responsiveHandleWidth: 50
            ,
            onFullscreenError: fullscreenErrorHandler
        };

        var books = {
            "#book": optionsbook
        };
        $("#book1-trigger, #book-trigger, #book3-trigger").on("click", function () {
            buildBook("#" + this.id.replace("-trigger", ""));
        })

        function buildBook(elem) {
            var book = $.wowBook(elem);
            if (!book) {
                $(elem).wowBook(books[elem]);
                book = $.wowBook(elem);
            }
            // book.opts.onHideLightbox = function(){
            //     setTimeout( function(){ book.destroy(); }, 1000);
            // }
            book.showLightbox();
        }


    });
</script>


<div class="wowbook-lightbox lightbox-pdf" style="width: 100%; height: 100%; display: none; left: 0px; top: 0px;">
    <div class="wowbook-toolbar wowbook-fontawesome" style="bottom: 0px;">
        <div class="wowbook-controls"><a class="wowbook-control wowbook-control-toggle-toolbar wowbook-collapsed"
                                         style="display: none;"><i></i></a><a
                    class="wowbook-control wowbook-control-lastLeft"><i></i></a><a
                    class="wowbook-control wowbook-control-left"><i></i></a><span
                    class="wowbook-control wowbook-control-currentPage" style="width: 43px;"><input
                        class="wowbook-input-page" style="display: none; top: 9px;"><span class="wowbook-current-page"
                                                                                          style="width: 100%; visibility: visible;">15/89</span></span><a
                    class="wowbook-control wowbook-control-right"><i></i></a><a
                    class="wowbook-control wowbook-control-lastRight"><i></i></a><a title="find"
                                                                                    class="wowbook-control wowbook-control-find"><i></i></a><a
                    title="table of contents on/off" class="wowbook-control wowbook-control-toc"><i></i></a><a
                    title="zoom in" class="wowbook-control wowbook-control-zoomin"><i></i></a><a title="zoom out"
                                                                                                 class="wowbook-control wowbook-control-zoomout"><i></i></a><a
                    title="download" class="wowbook-control wowbook-control-download"
                    href="https://previews.envatousercontent.com/files/229960506/img/aesop_fables/Aesops-Fables.pdf"
                    download="./img/aesop_fables/Aesops-Fables.pdf"><i></i></a><a title="flip sound on/off"
                                                                                  class="wowbook-control wowbook-control-flipsound"><i></i></a><a
                    title="fullscreen on/off" class="wowbook-control wowbook-control-fullscreen"><i></i></a><a
                    title="thumbnails on/off" class="wowbook-control wowbook-control-thumbnails"><i></i></a>
            <div id="findbar" class="wowbook-findbar wowbook-hidden"><label style="display:none">Find: </label>
                <div class="wowbook-find-text-container"><input class="wowbook-find-text"
                                                                placeholder="Text to find"><span
                            class="wowbook-find-count"></span></div>
                <a title="Find the previous occurrence of the phrase"
                   class="wowbook-find-previous wowbook-control-back"><i></i><span>Previous</span></a><a
                        title="Find the next occurrence of the phrase"
                        class="wowbook-find-next wowbook-control-next"><i></i><span>Next</span></a><label
                        style="display:none"><input class="wowbook-find-highlight-all" checked="checked"
                                                    type="checkbox"> Highlight all</label><label><input
                            class="wowbook-find-match-case" type="checkbox"> <span>Match case</span></label><a
                        class="wowbook-close"><i></i></a></div>
        </div>
    </div>
    <div class="wowbook-book-container" style="height: 613px; bottom: 46px;">
        <div id="book" class="wowbook wowbook-pdf" style="height: 573px; width: 764px;">
            <div class="wowbook-zoomwindow" style="position: absolute; top: 0px; width: 764px; height: 573px;">
                <div class="wowbook-zoomcontent wowbook-dragging"
                     style="position: absolute; left: 0px; top: 0px; touch-action: none; -moz-user-select: text; width: 860px; height: 645px; transform: scale(0.888372); transform-origin: 0px 0px 0px;">
                    <div class="wowbook-clipper"
                         style="position: absolute; left: 0px; top: 0px; width: 100%; overflow: hidden; z-index: 1; height: 645px;">
                        <div class="wowbook-inner-clipper"
                             style="position: absolute; width: 100%; overflow: hidden; height: 907px; top: -131px;">
                            <div class="wowbook-origin"
                                 style="position: absolute; width: 100%; height: 645px; top: 131px; left: 0px;">
                                <div class="wowbook-shadow-clipper"
                                     style="display: none; width: 860px; height: 645px; top: 0px; left: 0px;">
                                    <div class="wowbook-shadow-container"
                                         style="position: relative; display: none; opacity: 0.0868565;">
                                        <div class="wowbook-shadow-internal"
                                             style="display: block; height: 1552px; transform: translate(831.619px, -453.5px) rotate(178.878deg); transform-origin: -186.619px 776px 0px;"></div>
                                        <div class="wowbook-shadow-fold"
                                             style="display: block; height: 1552px; transform-origin: -86.6191px 776px 0px;"></div>
                                    </div>
                                </div>
                                <div class="wowbook-hard-page-dropshadow"
                                     style="display: none; width: 430px; height: 645px; top: 0px; left: 0px; opacity: 0.4;"></div>
                                <div class="wowbook-handle wowbook-left" style="width: 56.2827px;"></div>
                                <div class="wowbook-handle wowbook-right"
                                     style="left: 803.717px; width: 56.2827px;"></div>
                                <div class="wowbook-page wowbook-hardpage wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 88; perspective-origin: 0px 50%; clip: auto; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-hardpage wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 1; perspective-origin: 0px 50%; clip: auto; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                    <div class="wowbook-hard-page-shadow" style="opacity: 0; display: none;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 86; clip: auto; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; position: absolute; left: 0px; top: 0px; z-index: 3; clip: auto; display: none; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 84; clip: auto; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; position: absolute; left: 0px; top: 0px; z-index: 5; clip: auto; display: none; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 82; clip: auto; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; position: absolute; left: 0px; top: 0px; z-index: 7; clip: auto; display: none; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 80; clip: auto; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; position: absolute; left: 0px; top: 0px; z-index: 9; clip: auto; display: none; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;">
                                        <div class="wowbook-gutter-shadow" style="height: 645px;"></div>
                                        <div class="wowbook-pdf-annotations"></div>
                                        <div class="wowbook-pdf-text">
                                            <div style="left: 327.246px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.07744);">
                                                Aesop’s Fables
                                            </div>
                                            <div style="left: 68.8051px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(0.946);">
                                                10
                                            </div>
                                            <div style="left: 66.5433px; top: 63.8542px; font-size: 26.6667px; font-family: serif; transform: scaleX(1.0201);">
                                                The Fox and the Crow
                                            </div>
                                            <div style="left: 68.404px; top: 146.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.0095);">
                                                A Fox once saw a Crow fly off with a piece of cheese in its
                                            </div>
                                            <div style="left: 70.1253px; top: 162.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.926259);">
                                                beak and settle on a branch of a tree. ‘That’s for me, as I
                                            </div>
                                            <div style="left: 70.1667px; top: 178.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.929323);">
                                                am a Fox,’ said Master Reynard, and he walked up to the
                                            </div>
                                            <div style="left: 69.8587px; top: 194.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00323);">
                                                foot of the tree. ‘Good-day, Mistress Crow,’ he cried. ‘How
                                            </div>
                                            <div style="left: 69.4373px; top: 210.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00129);">
                                                well you are looking to-day: how glossy your feathers; how
                                            </div>
                                            <div style="left: 70.1253px; top: 226.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02094);">
                                                bright your eye. I feel sure your voice must surpass that of
                                            </div>
                                            <div style="left: 70.04px; top: 242.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.949257);">
                                                other birds, just as your figure does; let me hear but one
                                            </div>
                                            <div style="left: 70.14px; top: 258.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01482);">
                                                song from you that I may greet you as the Queen of Birds.’
                                            </div>
                                            <div style="left: 68.48px; top: 274.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01076);">
                                                The Crow lifted up her head and began to caw her best, but
                                            </div>
                                            <div style="left: 69.8307px; top: 290.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02642);">
                                                the moment she opened her mouth the piece of cheese fell
                                            </div>
                                            <div style="left: 69.8307px; top: 306.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02065);">
                                                to the ground, only to be snapped up by Master Fox. ‘That
                                            </div>
                                            <div style="left: 69.4373px; top: 322.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.96714);">
                                                will do,’ said he. ‘That was all I wanted. In exchange for your
                                            </div>
                                            <div style="left: 70.04px; top: 338.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.904423);">
                                                cheese I will give you a piece of advice for the future .’Do
                                            </div>
                                            <div style="left: 69.9907px; top: 354.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00881);">
                                                not trust flatterers.’
                                            </div>
                                            <div class="endOfContent"></div>
                                        </div>
                                        <canvas class="wowbook-pdf-zoom-0.8883720930232558"
                                                style="transform: scale(1.12565); transform-origin: 0px 0px 0px;"
                                                height="573" width="381"></canvas>
                                    </div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 78; clip: auto; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;">
                                        <div class="wowbook-gutter-shadow" style="height: 645px;"></div>
                                        <div class="wowbook-pdf-annotations"></div>
                                        <div class="wowbook-pdf-text">
                                            <div style="left: 352.459px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(0.989091);">
                                                11
                                            </div>
                                            <div style="left: 41.7019px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.14111);">
                                                Free eBooks at
                                            </div>
                                            <div style="left: 108.293px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.15625);">
                                                Planet eBook.com
                                            </div>
                                            <div style="left: 38.686px; top: 63.8542px; font-size: 26.6667px; font-family: serif; transform: scaleX(0.949093);">
                                                The Sick Lion
                                            </div>
                                            <div style="left: 40.5467px; top: 146.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.919333);">
                                                A Lion had come to the end of his days and lay sick unto
                                            </div>
                                            <div style="left: 42.1693px; top: 162.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01987);">
                                                death at the mouth of his cave, gasping for breath. The ani
                                            </div>
                                            <div style="left: 358.82543999999996px; top: 162.74749066666672px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 42.0013px; top: 178.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.0349);">
                                                mals, his subjects, came round him and drew nearer as he
                                            </div>
                                            <div style="left: 42.2893px; top: 194.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.03529);">
                                                grew more and more helpless. When they saw him on the
                                            </div>
                                            <div style="left: 42.1347px; top: 210.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00116);">
                                                point of death they thought to themselves: ‘Now is the time
                                            </div>
                                            <div style="left: 41.9733px; top: 226.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.912209);">
                                                to pay off old grudges.’ So the Boar came up and drove at
                                            </div>
                                            <div style="left: 42.1213px; top: 242.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.936739);">
                                                him with his tusks; then a Bull gored him with his horns;
                                            </div>
                                            <div style="left: 42.2827px; top: 258.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.934103);">
                                                still the Lion lay helpless before them: so the Ass, feeling
                                            </div>
                                            <div style="left: 42.1693px; top: 274.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.976096);">
                                                quite safe from danger, came up, and turning his tail to
                                            </div>
                                            <div style="left: 41.9733px; top: 290.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.0096);">
                                                the Lion kicked up his heels into his face. ‘This is a double
                                            </div>
                                            <div style="left: 42.1693px; top: 306.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.987098);">
                                                death,’ growled the Lion.
                                            </div>
                                            <div style="left: 57.9333px; top: 322.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00873);">
                                                Only cowards insult dying majesty.
                                            </div>
                                            <div class="endOfContent"></div>
                                        </div>
                                        <canvas class="wowbook-pdf-zoom-0.8883720930232558"
                                                style="transform: scale(1.12565); transform-origin: 0px 0px 0px;"
                                                height="573" width="381"></canvas>
                                    </div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; position: absolute; left: 0px; top: 0px; z-index: 11; clip: auto; display: block; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;">
                                        <div class="wowbook-gutter-shadow" style="height: 645px;"></div>
                                        <div class="wowbook-pdf-annotations"></div>
                                        <div class="wowbook-pdf-text">
                                            <div style="left: 327.246px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.07744);">
                                                Aesop’s Fables
                                            </div>
                                            <div style="left: 68.80597333333333px; top: 547.867744px; font-size: 9.333333333333332px; font-family: serif;">
                                                1
                                            </div>
                                            <div style="left: 73.31397333333332px; top: 547.867744px; font-size: 9.333333333333332px; font-family: serif;">
                                                
                                            </div>
                                            <div style="left: 66.5433px; top: 63.8542px; font-size: 26.6667px; font-family: serif; transform: scaleX(1.00645);">
                                                The Ass and the Lapdog
                                            </div>
                                            <div style="left: 68.404px; top: 146.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.92874);">
                                                A Farmer one day came to the stables to see to his beasts
                                            </div>
                                            <div style="left: 70.04px; top: 162.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.932797);">
                                                of burden: among them was his favourite Ass, that was al
                                            </div>
                                            <div style="left: 386.68277333333333px; top: 162.74749066666672px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 69.4373px; top: 178.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.999876);">
                                                ways well fed and often carried his master. With the Farmer
                                            </div>
                                            <div style="left: 70.04px; top: 194.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.958558);">
                                                came his Lapdog, who danced about and licked his hand
                                            </div>
                                            <div style="left: 70.1667px; top: 210.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02353);">
                                                and frisked about as happy as could be. The Farmer felt in
                                            </div>
                                            <div style="left: 69.9787px; top: 226.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.9842);">
                                                his pocket, gave the Lapdog some dainty food, and sat down
                                            </div>
                                            <div style="left: 69.4373px; top: 242.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.982124);">
                                                while he gave his orders to his servants. The Lapdog jumped
                                            </div>
                                            <div style="left: 69.9507px; top: 258.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00671);">
                                                into his master’s lap, and lay there blinking while the Farm
                                            </div>
                                            <div style="left: 386.68277333333333px; top: 258.7474906666667px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 70.1933px; top: 274.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.929816);">
                                                er stroked his ears. The Ass, seeing this, broke loose from
                                            </div>
                                            <div style="left: 69.9787px; top: 290.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.96673);">
                                                his halter and commenced prancing about in imitation of
                                            </div>
                                            <div style="left: 69.8307px; top: 306.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.994291);">
                                                the Lapdog. The Farmer could not hold his sides with laugh
                                            </div>
                                            <div style="left: 386.68277333333333px; top: 306.74749066666664px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 69.8307px; top: 322.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.941032);">
                                                ter, so the Ass went up to him, and putting his feet upon
                                            </div>
                                            <div style="left: 69.8307px; top: 338.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.0237);">
                                                the Farmer’s shoulder attempted to climb into his lap. The
                                            </div>
                                            <div style="left: 69.524px; top: 354.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01364);">
                                                Farmer’s servants rushed up with sticks and pitchforks and
                                            </div>
                                            <div style="left: 70.14px; top: 370.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00541);">
                                                soon taught the Ass that clumsy jesting is no joke.
                                            </div>
                                            <div class="endOfContent"></div>
                                        </div>
                                        <canvas class="wowbook-pdf-zoom-0.8883720930232558"
                                                style="transform: scale(1.12565); transform-origin: 0px 0px 0px;"
                                                height="573" width="381"></canvas>
                                    </div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 76; clip: auto; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;">
                                        <div class="wowbook-gutter-shadow" style="height: 645px;"></div>
                                        <div class="wowbook-pdf-annotations"></div>
                                        <div class="wowbook-pdf-text">
                                            <div style="left: 352.56717333333336px; top: 547.867744px; font-size: 9.333333333333332px; font-family: serif;">
                                                1
                                            </div>
                                            <div style="left: 356.47793333333334px; top: 547.867744px; font-size: 9.333333333333332px; font-family: serif;">
                                                
                                            </div>
                                            <div style="left: 41.7019px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.14111);">
                                                Free eBooks at
                                            </div>
                                            <div style="left: 108.293px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.15625);">
                                                Planet eBook.com
                                            </div>
                                            <div style="left: 38.686px; top: 63.8542px; font-size: 26.6667px; font-family: serif; transform: scaleX(1.00181);">
                                                The Lion and the Mouse
                                            </div>
                                            <div style="left: 41.9333px; top: 146.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00778);">
                                                Once when a Lion was asleep a little Mouse began running
                                            </div>
                                            <div style="left: 41.9747px; top: 162.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.03663);">
                                                up and down upon him; this soon wakened the Lion, who
                                            </div>
                                            <div style="left: 42.1347px; top: 178.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02006);">
                                                placed his huge paw upon him, and opened his big jaws to
                                            </div>
                                            <div style="left: 42.2827px; top: 194.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00394);">
                                                swallow him. ‘Pardon, O King,’ cried the little Mouse: ‘for
                                            </div>
                                            <div style="left: 358.82543999999996px; top: 194.74749066666672px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 42.2893px; top: 210.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.947078);">
                                                give me this time, I shall never forget it: who knows but
                                            </div>
                                            <div style="left: 41.58px; top: 226.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.923647);">
                                                what I may be able to do you a turn some of these days?’
                                            </div>
                                            <div style="left: 40.6227px; top: 242.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00103);">
                                                The Lion was so tickled at the idea of the Mouse being able
                                            </div>
                                            <div style="left: 41.9733px; top: 258.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01432);">
                                                to help him, that he lifted up his paw and let him go. Some
                                            </div>
                                            <div style="left: 41.9733px; top: 274.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.950323);">
                                                time after the Lion was caught in a trap, and the hunters
                                            </div>
                                            <div style="left: 41.58px; top: 290.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.937923);">
                                                who desired to carry him alive to the King, tied him to a
                                            </div>
                                            <div style="left: 41.9733px; top: 306.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00839);">
                                                tree while they went in search of a waggon to carry him on.
                                            </div>
                                            <div style="left: 41.5093px; top: 322.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.03477);">
                                                Just then the little Mouse happened to pass by, and seeing
                                            </div>
                                            <div style="left: 41.9733px; top: 338.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.926267);">
                                                the sad plight in which the Lion was, went up to him and
                                            </div>
                                            <div style="left: 42.2827px; top: 354.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.96048);">
                                                soon gnawed away the ropes that bound the King of the
                                            </div>
                                            <div style="left: 41.72px; top: 370.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.989932);">
                                                Beasts. ‘Was I not right?’ said the little Mouse.
                                            </div>
                                            <div style="left: 57.72px; top: 386.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00302);">
                                                Little friends may prove great friends.
                                            </div>
                                            <div class="endOfContent"></div>
                                        </div>
                                        <canvas class="wowbook-pdf-zoom-0.8883720930232558"
                                                style="transform: scale(1.12565); transform-origin: 0px 0px 0px;"
                                                height="573" width="381"></canvas>
                                    </div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; position: absolute; left: 0px; top: 0px; z-index: 13; clip: auto; display: block; margin-left: 0px; margin-top: 0px;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px; clip: auto; margin-left: 0px; margin-top: 0px;">
                                        <div class="wowbook-gutter-shadow" style="height: 645px;"></div>
                                        <div class="wowbook-pdf-annotations"></div>
                                        <div class="wowbook-pdf-text">
                                            <div style="left: 327.246px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.07744);">
                                                Aesop’s Fables
                                            </div>
                                            <div style="left: 68.80597333333333px; top: 547.867744px; font-size: 9.333333333333332px; font-family: serif;">
                                                1
                                            </div>
                                            <div style="left: 73.43530666666666px; top: 547.867744px; font-size: 9.333333333333332px; font-family: serif;">
                                                
                                            </div>
                                            <div style="left: 66.5433px; top: 63.8542px; font-size: 26.6667px; font-family: serif; transform: scaleX(1.03394);">
                                                The Swallow and the
                                            </div>
                                            <div style="left: 69.7913px; top: 95.8542px; font-size: 26.6667px; font-family: serif; transform: scaleX(1.09088);">
                                                Other Birds
                                            </div>
                                            <div style="left: 69.5507px; top: 178.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.987238);">
                                                It happened that a Countryman was sowing some hemp
                                            </div>
                                            <div style="left: 70.14px; top: 194.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.99468);">
                                                seeds in a field where a Swallow and some other birds were
                                            </div>
                                            <div style="left: 69.9787px; top: 210.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01314);">
                                                hopping about picking up their food. ‘Beware of that man,’
                                            </div>
                                            <div style="left: 70.0267px; top: 226.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.98784);">
                                                quoth the Swallow. ‘Why, what is he doing?’ said the others.
                                            </div>
                                            <div style="left: 67.684px; top: 242.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.999496);">
                                                ‘That is hemp seed he is sowing; be careful to pick up every
                                            </div>
                                            <div style="left: 70.04px; top: 258.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.90719);">
                                                one of the seeds, or else you will repent it.’ The birds paid
                                            </div>
                                            <div style="left: 69.9907px; top: 274.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.931765);">
                                                no heed to the Swallow’s words, and by and by the hemp
                                            </div>
                                            <div style="left: 70.1467px; top: 290.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.99241);">
                                                grew up and was made into cord, and of the cords nets were
                                            </div>
                                            <div style="left: 69.8587px; top: 306.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01807);">
                                                made, and many a bird that had despised the Swallow’s ad
                                            </div>
                                            <div style="left: 386.68277333333333px; top: 306.74749066666664px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 69.412px; top: 322.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01407);">
                                                vice was caught in nets made out of that very hemp. ‘What
                                            </div>
                                            <div style="left: 70.0267px; top: 338.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.974903);">
                                                did I tell you?’ said the Swallow.
                                            </div>
                                            <div style="left: 85.604px; top: 354.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00413);">
                                                Destroy the seed of evil, or it will grow up to your ruin.
                                            </div>
                                            <div class="endOfContent"></div>
                                        </div>
                                        <canvas class="wowbook-pdf-zoom-0.8883720930232558"
                                                style="transform: scale(1.12565); transform-origin: 0px 0px 0px;"
                                                height="573" width="381"></canvas>
                                    </div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: block; position: absolute; left: 430px; top: 0px; z-index: 74;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;">
                                        <div class="wowbook-gutter-shadow" style="height: 645px;"></div>
                                        <div class="wowbook-pdf-annotations"></div>
                                        <div class="wowbook-pdf-text">
                                            <div style="left: 352.54386666666664px; top: 547.867744px; font-size: 9.333333333333332px; font-family: serif;">
                                                1
                                            </div>
                                            <div style="left: 356.44519999999994px; top: 547.867744px; font-size: 9.333333333333332px; font-family: serif;">
                                                
                                            </div>
                                            <div style="left: 41.7019px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.14111);">
                                                Free eBooks at
                                            </div>
                                            <div style="left: 108.293px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.15625);">
                                                Planet eBook.com
                                            </div>
                                            <div style="left: 38.686px; top: 63.8542px; font-size: 26.6667px; font-family: serif; transform: scaleX(1.00277);">
                                                The Frogs Desiring a King
                                            </div>
                                            <div style="left: 40.6227px; top: 146.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.95762);">
                                                The Frogs were living as happy as could be in a marshy
                                            </div>
                                            <div style="left: 42.2827px; top: 162.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00775);">
                                                swamp that just suited them; they went splashing about car
                                            </div>
                                            <div style="left: 358.82543999999996px; top: 162.74749066666672px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 42.0933px; top: 178.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01741);">
                                                ing for nobody and nobody troubling with them. But some
                                            </div>
                                            <div style="left: 42.1827px; top: 194.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.964949);">
                                                of them thought that this was not right, that they should
                                            </div>
                                            <div style="left: 42.1213px; top: 210.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.03812);">
                                                have a king and a proper constitution, so they determined
                                            </div>
                                            <div style="left: 41.9733px; top: 226.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.999402);">
                                                to send up a petition to Jove to give them what they wanted.
                                            </div>
                                            <div style="left: 39.8267px; top: 242.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.995518);">
                                                ‘Mighty Jove,’ they cried, ‘send unto us a king that will rule
                                            </div>
                                            <div style="left: 42.1827px; top: 258.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.985475);">
                                                over us and keep us in order.’ Jove laughed at their croaking,
                                            </div>
                                            <div style="left: 42.3093px; top: 274.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.938697);">
                                                and threw down into the swamp a huge Log, which came
                                            </div>
                                            <div style="left: 42.1693px; top: 290.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00339);">
                                                downrplashto the swamp. The Frogs were frightened out of
                                            </div>
                                            <div style="left: 41.9733px; top: 306.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.946788);">
                                                their lives by the commotion made in their midst, and all
                                            </div>
                                            <div style="left: 42.08px; top: 322.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.93906);">
                                                rushed to the bank to look at the horrible monster; but af
                                            </div>
                                            <div style="left: 358.82543999999996px; top: 322.74749066666664px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 41.9733px; top: 338.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.93459);">
                                                ter a time, seeing that it did not move, one or two of the
                                            </div>
                                            <div style="left: 42.268px; top: 354.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.969403);">
                                                boldest of them ventured out towards the Log, and even
                                            </div>
                                            <div style="left: 42.1693px; top: 370.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.953176);">
                                                dared to touch it; still it did not move. Then the greatest
                                            </div>
                                            <div style="left: 42.1213px; top: 386.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.963966);">
                                                hero of the Frogs jumped upon the Log and commenced
                                            </div>
                                            <div style="left: 42.1693px; top: 402.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00196);">
                                                dancing up and down upon it, thereupon all the Frogs came
                                            </div>
                                            <div style="left: 42.3093px; top: 418.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02712);">
                                                and did the same; and for some time the Frogs went about
                                            </div>
                                            <div style="left: 41.9733px; top: 434.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01582);">
                                                their business every day without taking the slightest notice
                                            </div>
                                            <div style="left: 42.1827px; top: 450.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01317);">
                                                of their new King Log lying in their midst. But this did not
                                            </div>
                                            <div style="left: 42.2827px; top: 466.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.95696);">
                                                suit them, so they sent another petition to Jove, and said
                                            </div>
                                            <div style="left: 41.9733px; top: 482.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02632);">
                                                to him, ‘We want a real king; one that will really rule over
                                            </div>
                                            <div style="left: 41.9747px; top: 498.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.98493);">
                                                us.’ Now this made Jove angry, so he sent among them a big
                                            </div>
                                            <div style="left: 42.1293px; top: 514.747px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01955);">
                                                Stork that soon set to work gobbling them all up. Then the
                                            </div>
                                            <div class="endOfContent"></div>
                                        </div>
                                        <canvas class="wowbook-pdf-zoom-0.8883720930232558"
                                                style="transform: scale(1.12565); transform-origin: 0px 0px 0px;"
                                                height="573" width="381"></canvas>
                                    </div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 15;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;">
                                        <canvas class="wowbook-pdf-zoom-0.8883720930232558"
                                                style="transform: scale(1.12565); transform-origin: 0px 0px 0px;"
                                                height="573" width="381"></canvas>
                                        <div class="wowbook-gutter-shadow" style="height: 645px;"></div>
                                        <div class="wowbook-pdf-annotations"></div>
                                        <div class="wowbook-pdf-text">
                                            <div style="left: 327.246px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.07744);">
                                                Aesop’s Fables
                                            </div>
                                            <div style="left: 68.80597333333333px; top: 547.868024px; font-size: 9.333333333333332px; font-family: serif;">
                                                1
                                            </div>
                                            <div style="left: 73.08997333333332px; top: 547.868024px; font-size: 9.333333333333332px; font-family: serif;">
                                                
                                            </div>
                                            <div style="left: 69.524px; top: 66.7479px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00304);">
                                                Frogs repented when too late.
                                            </div>
                                            <div style="left: 85.5773px; top: 82.7479px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.0267);">
                                                Better no rule than cruel rule.
                                            </div>
                                            <div class="endOfContent"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: block; position: absolute; left: 430px; top: 0px; z-index: 72;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;">
                                        <canvas class="wowbook-pdf-zoom-0.8883720930232558"
                                                style="transform: scale(1.12565); transform-origin: 0px 0px 0px;"
                                                height="573" width="381"></canvas>
                                        <div class="wowbook-gutter-shadow" style="height: 645px;"></div>
                                        <div class="wowbook-pdf-annotations"></div>
                                        <div class="wowbook-pdf-text">
                                            <div style="left: 352.5616px; top: 547.868024px; font-size: 9.333333333333332px; font-family: serif;">
                                                1
                                            </div>
                                            <div style="left: 356.6682666666666px; top: 547.868024px; font-size: 9.333333333333332px; font-family: serif;">
                                                
                                            </div>
                                            <div style="left: 41.7019px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.14111);">
                                                Free eBooks at
                                            </div>
                                            <div style="left: 108.293px; top: 547.868px; font-size: 9.33333px; font-family: serif; transform: scaleX(1.15625);">
                                                Planet eBook.com
                                            </div>
                                            <div style="left: 38.686px; top: 63.8546px; font-size: 26.6667px; font-family: serif; transform: scaleX(1.03086);">
                                                The Mountains in Labour
                                            </div>
                                            <div style="left: 41.9333px; top: 146.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02526);">
                                                One day the Countrymen noticed that the Mountains were
                                            </div>
                                            <div style="left: 42.0933px; top: 162.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.0265);">
                                                in labour; smoke came out of their summits, the earth was
                                            </div>
                                            <div style="left: 42.1693px; top: 178.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.950897);">
                                                quaking at their feet, trees were crashing, and huge rocks
                                            </div>
                                            <div style="left: 41.58px; top: 194.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.02558);">
                                                were tumbling. They felt sure that something horrible was
                                            </div>
                                            <div style="left: 42.2893px; top: 210.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00928);">
                                                going to happen. They all gathered together in one place to
                                            </div>
                                            <div style="left: 42.2827px; top: 226.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01891);">
                                                see what terrible thing this could be. They waited and they
                                            </div>
                                            <div style="left: 41.58px; top: 242.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00866);">
                                                waited, but nothing came. At last there was a still more vio
                                            </div>
                                            <div style="left: 358.82543999999996px; top: 242.74790399999998px; font-size: 13.333333333333332px; font-family: serif;">
                                                -
                                            </div>
                                            <div style="left: 42.1613px; top: 258.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01538);">
                                                lent earthquake, and a huge gap appeared in the side of the
                                            </div>
                                            <div style="left: 41.6893px; top: 274.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00693);">
                                                Mountains. They all fell down upon their knees and waited.
                                            </div>
                                            <div style="left: 40.5467px; top: 290.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.01814);">
                                                At last, and at last, a teeny, tiny mouse poked its little head
                                            </div>
                                            <div style="left: 42.3093px; top: 306.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.00527);">
                                                and bristles out of the gap and came running down towards
                                            </div>
                                            <div style="left: 41.9733px; top: 322.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(1.0039);">
                                                them, and ever after they used to say:
                                            </div>
                                            <div style="left: 55.8267px; top: 338.748px; font-size: 13.3333px; font-family: serif; transform: scaleX(0.98776);">
                                                ‘Much outcry, little outcome.’
                                            </div>
                                            <div class="endOfContent"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 17;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 70;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 19;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 68;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 21;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 66;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 23;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 64;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 25;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 62;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 27;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 60;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 29;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 58;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 31;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 56;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 33;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 54;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 35;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 52;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 37;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 50;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 39;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 48;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 41;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 46;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 43;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 44;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 45;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 42;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 47;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 40;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 49;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 38;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 51;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 36;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 53;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 34;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 55;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 32;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 57;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 30;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 59;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 28;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 61;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 26;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 63;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 24;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 65;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 22;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 67;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 20;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 69;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 18;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 71;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 16;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 73;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 14;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 75;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 12;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 77;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 10;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 79;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 8;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 81;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 6;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 83;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 4;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 85;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 2;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-left"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 0px; top: 0px; z-index: 87;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-page wowbook-hardpage wowbook-right"
                                     style="width: 430px; height: 645px; display: none; position: absolute; left: 430px; top: 0px; z-index: 0;">
                                    <div class="wowbook-page-content"
                                         style="box-sizing: border-box; width: 430px; height: 645px;"></div>
                                </div>
                                <div class="wowbook-fold-gradient-container"
                                     style="display: none; position: absolute; width: 125px; height: 1552px; top: 0px; left: 0px; opacity: 0.00702613;">
                                    <div class="wowbook-fold-gradient" style="height: 1552px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wowbook-book-shadow"
                         style="top: 0px; position: absolute; z-index: 0; width: 860px; height: 645.067px; transform: translate(0px, 0px);"></div>
                </div>
            </div>
        </div>
    </div>
    <button class="wowbook-close">✕</button>
</div>
</body>
</html>