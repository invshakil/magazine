@extends('frontend.master')

@section('title')
    Gallery
@endsection

@section('main_content')

    <div id="#content" class="site-content">
        <div class="container">

            <div class="row">
                @foreach($latest as $item)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="product-single">
                            <div class="product-thumb">
                                <img src="{{ url($item->image) }}" width="220" class="img-responsive">
                                <div class="actions">
                                    <ul>
                                        <li><a class="add-cart"
                                               href="{{url('/magazine-details/'.base64_encode($item->magazine_id).'/'.str_replace(' ','-',$item->name) )}}"><span><span
                                                            class="fa fa-check-circle"></span></span>{{ $item->name }}</a>
                                        </li>

                                        <li class="pull-right"><a class="zoom"
                                                                  href="{{URL::to ($item->image)}}"><span
                                                        class="arrow_expand"></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>


        </div>


    </div>


    {{--@foreach($latest as $item)--}}
        {{--<div id="myModal{{ $item->magazine_id }}" class="modal fade" role="dialog">--}}
            {{--<div class="modal-dialog modal-lg">--}}

                {{--<!-- Modal content-->--}}
                {{--<div class="modal-content">--}}
                    {{--<div class="modal-header">--}}
                        {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                        {{--<h4 class="modal-title">{{ $item->name }}</h4>--}}
                    {{--</div>--}}
                    {{--<div class="modal-body">--}}
                        {{--<img src="{{ $item->image }}" height="400px" class="img-responsive" id="image">--}}
                    {{--</div>--}}

                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
        {{--</div>--}}
    {{--@endforeach--}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script type="text/javascript">

        $('#myModal').on('show.bs.modal', function (e) {
            var image = $(e.relatedTarget).data('img'); //<- It's data-image value
            var name = $(e.relatedTarget).data('name'); //<- It's data-name value
            console.log(1);
            console.log(image);

            $('img#image').attr('src', '{{ URL::to('/') }}/' + image);
        });

    </script>
@endsection