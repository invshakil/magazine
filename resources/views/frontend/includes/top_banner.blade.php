<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="logo">
                    <a href="{{url('/')}}"><img src="{{URL::to ('public/web_assets')}}/assets/images/logo.png" width="100"
                                                alt="logo"/>
                    </a>
                </div>

                <!--/.left-->
            </div>
            <!--/.col-md-6-->
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="right"> Editor: Iqbal Kabir Mohon <br/>
                    <span><i class="fa fa-phone"></i>
                        Call us</span> 01928-686294
                </div>
                <!--/.right-->
            </div>
            <!--/.col-md-6-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>