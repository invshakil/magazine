<div class="navbar groham-nav megamenu" style="margin-top: 30px;">
    {{--<div class="container">--}}
        <div class="col-md-8 col-sm-8 col-xs-12 site-logo">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse"
                        aria-expanded="false"><span class="sr-only">Toggle navigation</span> <span
                            class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{url('/')}}" >Home</a>
                    </li>

                    <li><a href="{{url('/about-us')}}">About Us</a>
                    </li>

                    <li><a href="{{ url('/gallery') }}">Gallery</a>
                    </li>

                    <li><a href="{{ url('/contact') }}">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 header-search">
            <div class="search default">
                <form class="searchform" action="#" method="get">
                    <div class="input-group">
                        <input type="search" id="dsearch" name="s" class="form-control"
                               placeholder="Search for something..."> <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" id="submit-btn"><span
                                                    class="arrow_right"></span>
                                    </button>
                                    </span>
                    </div>
                </form>
            </div>
            <!--/.search-->
        </div>
    {{--</div>--}}
    <!-- /.container-fluid -->
</div>