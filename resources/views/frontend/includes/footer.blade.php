<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="widget widget-contect">
                        <h3 class="widget-title">Contact Information</h3>
                        <p>Print Copy Price: 50tk per edition</p>
                        <p><strong>Address</strong> Sishu Kanon, 307 Ulon Road, East Rampura, Dhaka-1219</p>
                        <p><strong>Phone:</strong> +8801928-686294</p>
                        <a href="mailto:ikmohon@gmail.com">ikmohon@gmail.com</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="widget widget-services">

                        <iframe height="250" src="https://www.youtube.com/embed/e0BJtZbPoSg" frameborder="0" allowfullscreen></iframe>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="widget widget-tag">
                        <iframe height="250" src="https://www.youtube.com/embed/dQBsodMhbkk" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="widget widget-tag">
                        <iframe height="250" src="https://www.youtube.com/embed/x29mCSQ--uE" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--/.footer-top-->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="footer-logo">
                    <a href="home01.html"><img src="{{URL::to ('public/web_assets')}}/assets/images/logo.png" width="150"
                                               alt="logo">
                    </a>
                </div>
                <ul class="social">
                    <li><a href="#">facebook</a>
                    </li>
                    <li><a href="#">twitter</a>
                    </li>
                    <li><a href="#">pinterest</a>
                    </li>
                    <li><a href="#">instagram</a>
                    </li>
                    <li><a href="#">linked in</a>
                    </li>
                    <li><a href="#">google+</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--/.footer-bottom-->
    <div class="copyright home01">
        <div class="container">
            <p class="text-center">&copy; Developed by <a href="http://techno-71.com" target="_blank">Techno-71</a> | All rights reserved</p>
        </div>
    </div>
    <!--/.copyright-->
</footer>